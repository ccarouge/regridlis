This repository contains a series of codes to prepare LIS inputs at NCI:

* Codes to regrid the LIS input files from 1KM to coarser resolutions. Although LDT can do the regridding on the fly, it can take some time to do so. We created several other resolutions to speed up the creation of the LIS input data by LDT for some often-used resolutions.
* Code to create the soil albedo input for CABLE: `convert_Houldcroft_soil_alb_corrected.ncl`
* Codes to create the 1KM maps of MODIS LAI: `interp_MODIS_LAI_1km.ncl` and `convert_2_monthly_ann.ncl`
* Code to convert the GSWP3 slope data from the CABLE gridinfo file: `GSWP3_slope.ncl`
* Code to remap the IGBP-NCEP land cover categories to the categories used by CABLE-NCAR: `remap_IGBP2CABLE.F90`
### How do I get set up? ###

The codes to regrid the input files and remap the land cover are written in Fortran. They have been run at the NCI using the Intel Fortran compiler. 

The regrid_all.csh script is set to:

* Set the paths and target resolution
* Load the Intel compiler module
* Compile the different codes
* Run each code sequentially

Although the codes are currently set to run sequentially, only the landmask needs to be done first, the other codes could be set to run in parallel.

The other codes are written in NCL. Those are not updated and should be used as a record of what was done.
### Questions ###

If you have any questions about this code, please use the issues.